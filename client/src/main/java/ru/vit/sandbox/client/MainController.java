package ru.vit.sandbox.client;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.vit.sandbox.common.entities.ResultOfSmth;
import ru.vit.sandbox.common.entities.XmlInfo;
import ru.vit.sandbox.common.entities.XmlResult;
import ru.vit.sandbox.common.services.IXmlService;
import ru.vit.sandbox.conf.ResourcesProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;

/**
 * Created by v.gryazev on 04.04.2017.
 */
@Controller
public class MainController {
    final static Logger logger = Logger.getLogger(MainController.class);
    @Autowired
    ResourcesProvider resourcesProvider;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("xmlModel", new XmlInfo());
        return "index";
    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody XmlResult validateXml(@ModelAttribute("validateForm")XmlInfo xmlInfo) {
        IXmlService service = (IXmlService) resourcesProvider.getService(IXmlService.class);

        Response response = service.validate(xmlInfo);
        XmlResult result;
        if (response.getStatus()==200) {
            result = response.readEntity(XmlResult.class);
        }else {
            result = new XmlResult();
            result.setValidateResult(new ResultOfSmth(false,"Ошибка сервиса валидации, код ошибки: " + response.getStatus()));
        }
        return result;
    }

    @RequestMapping(value = "/transform", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody XmlResult transformXml(@ModelAttribute("validateForm")XmlInfo xmlInfo) {
        IXmlService service = (IXmlService) resourcesProvider.getService(IXmlService.class);

        Response response = service.transform(xmlInfo);
        XmlResult result;
        if (response.getStatus()==200) {
            result = response.readEntity(XmlResult.class);
        }else {
            result = new XmlResult();
            result.setTransformResult(new ResultOfSmth(false,"Ошибка сервиса преобразования, код ошибки: " + response.getStatus()));
        }
        return result;
    }
}