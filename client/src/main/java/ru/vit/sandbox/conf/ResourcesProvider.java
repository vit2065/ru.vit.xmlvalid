package ru.vit.sandbox.conf;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import ru.vit.sandbox.common.services.IXmlService;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

/**
 * Created by v.gryazev on 05.04.2017.
 *
 */
@Configuration
@PropertySource("classpath:services.properties")
public class ResourcesProvider {
    @Autowired
    private Environment env;

    private HashMap <Class, Object> resources = new HashMap<>(); // перечень всех сервисов

    @PostConstruct
    private void init(){
        Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
        try {
            URI pathToService = new URI(env.getProperty("services.url"));
            resources.put(IXmlService.class, WebResourceFactory.newResource(IXmlService.class, client.target(pathToService)));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Object getService(Class clazz) {
        return resources.get(clazz);
    }
}
