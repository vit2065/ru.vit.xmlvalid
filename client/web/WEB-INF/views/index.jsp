<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Валидация и преобразование XML</title>
    <link href="${pageContext.request.contextPath}/resources/core/css/hello.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/resources/core/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <form:form method = "POST" action = "t" modelAttribute="xmlModel" id="validateForm">
            <div class="form-group">
                <label for="xmlTextArea">XML</label>
                <form:textarea path = "xml" class="form-control" id="xmlTextArea" rows="10"/>
            </div>

            <div class="form-group">
                <label for="xsdTextArea">XSD</label>
                <form:textarea path = "xsd" class="form-control" id="xsdTextArea" rows="5"/>
            </div>

            <div class="form-group">
                <label for="xslTextArea">XSL</label>
                <form:textarea path = "xsl" class="form-control" id="xslTextArea" rows="5"/>
            </div>
            <button class="btn btn-primary" formaction="validate">Валидировать и преобразовать</button>
            <button class="btn btn-primary" formaction="transform">Преобразовать</button>
        </form:form>
    </div>
    <div class="container" id="result"/>

    <script src="${pageContext.request.contextPath}/resources/core/js/jquery-3.2.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/core/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js-app/app.js"></script>

</body>
</html>