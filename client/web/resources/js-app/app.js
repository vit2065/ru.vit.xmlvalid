/**
 * Created by v.gryazev on 04.04.2017.
 */

// var $form = ;

// $("#validateForm").submit(function(e)
// {
//
//     e.preventDefault();
// });

$("#validateForm button").click(function(e){
    e.preventDefault();
    var postData = $('#validateForm').serializeArray();
    var formURL = $(this).attr("formaction");
    var parentDiv = $( "#result" );
    $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            dataType: "json",
            success:function(data)
            {
                setResponseDiv(data,parentDiv);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                parentDiv.html(jqXHR + " " + textStatus + " " + errorThrown);
            }
        });
});


function setResponseDiv(response, parent) {
    console.log(response);
    var validateResult = response["validateResult"];
    var transformResult = response["transformResult"];

    var html = '<h2>Результат</h2>';
    if (validateResult!== null) {
        if (validateResult["ok"] === true) {
            html += '<div class="panel panel-success">' + validateResult["result"] + '</div>';
        } else {
            html += '<div class="panel panel-danger">' + validateResult["result"] + '</div>';
        }
    }
    if (transformResult!==null) {
        html += '<textarea class="form-control" rows="10" readonly>' + transformResult["result"] + '</textarea>';
    }
    parent.html(html);
}

