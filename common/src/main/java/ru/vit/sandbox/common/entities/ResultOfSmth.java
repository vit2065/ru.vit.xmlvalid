package ru.vit.sandbox.common.entities;

/**
 * Created by v.gryazev on 10.04.2017.
 */
public class ResultOfSmth {
    private Boolean isOk;
    private String result;

    public ResultOfSmth() {
    }

    public ResultOfSmth(Boolean isOk, String result) {
        this.isOk = isOk;
        this.result = result;
    }

    public Boolean getOk() {
        return isOk;
    }

    public void setOk(Boolean ok) {
        isOk = ok;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResultOfSmth{" +
                "isOk=" + isOk +
                ", result='" + result + '\'' +
                '}';
    }
}
