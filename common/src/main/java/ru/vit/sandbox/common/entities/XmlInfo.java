package ru.vit.sandbox.common.entities;

/**
 * Created by v.gryazev on 04.04.2017.
 */
public class XmlInfo {
    private String xml;
    private String xsd;
    private String xsl;

    public XmlInfo() {
    }

    public XmlInfo(String xml, String xsd, String xsl) {
        this.xml = xml;
        this.xsd = xsd;
        this.xsl = xsl;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getXsd() {
        return xsd;
    }

    public void setXsd(String xsd) {
        this.xsd = xsd;
    }

    public String getXsl() {
        return xsl;
    }

    public void setXsl(String xsl) {
        this.xsl = xsl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XmlInfo xmlInfo = (XmlInfo) o;

        if (xml != null ? !xml.equals(xmlInfo.xml) : xmlInfo.xml != null) return false;
        if (xsd != null ? !xsd.equals(xmlInfo.xsd) : xmlInfo.xsd != null) return false;
        return xsl != null ? xsl.equals(xmlInfo.xsl) : xmlInfo.xsl == null;
    }

    @Override
    public int hashCode() {
        int result = xml != null ? xml.hashCode() : 0;
        result = 31 * result + (xsd != null ? xsd.hashCode() : 0);
        result = 31 * result + (xsl != null ? xsl.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "XmlInfo{" +
                "xml='" + xml + '\'' +
                ", xsd='" + xsd + '\'' +
                ", xsl='" + xsl + '\'' +
                '}';
    }
}
