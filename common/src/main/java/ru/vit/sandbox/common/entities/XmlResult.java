package ru.vit.sandbox.common.entities;


/**
 * Created by v.gryazev on 05.04.2017.
 */
public class XmlResult {
    private ResultOfSmth transformResult;
    private ResultOfSmth validateResult;
    private XmlInfo xmlInfo;

    public XmlResult() {
    }

    public XmlResult(XmlInfo xmlInfo) {
        this.xmlInfo = xmlInfo;
    }

    public ResultOfSmth getTransformResult() {
        return transformResult;
    }

    public void setTransformResult(ResultOfSmth transformResult) {
        this.transformResult = transformResult;
    }

    public ResultOfSmth getValidateResult() {
        return validateResult;
    }

    public void setValidateResult(ResultOfSmth validateResult) {
        this.validateResult = validateResult;
    }

    public XmlInfo getXmlInfo() {
        return xmlInfo;
    }

    public void setXmlInfo(XmlInfo xmlInfo) {
        this.xmlInfo = xmlInfo;
    }

    @Override
    public String toString() {
        return "XmlResult{" +
                "transformResult=" + transformResult +
                ", validateResult=" + validateResult +
                ", xmlInfo=" + xmlInfo +
                '}';
    }
}
