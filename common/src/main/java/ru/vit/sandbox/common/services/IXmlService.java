package ru.vit.sandbox.common.services;

import ru.vit.sandbox.common.entities.XmlInfo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by v.gryazev on 05.04.2017.
 */
public interface IXmlService {
    @POST
    @Path("/validate")
    @Consumes(MediaType.APPLICATION_JSON)
    Response validate(XmlInfo xmlInfo);

    @POST
    @Path("/transform")
    @Consumes(MediaType.APPLICATION_JSON)
    Response transform(XmlInfo xmlInfo);


    @GET
    Response test();
}
