package ru.vit.sandbox.services;

import org.apache.log4j.Logger;
import ru.vit.sandbox.common.entities.XmlInfo;
import ru.vit.sandbox.common.entities.XmlResult;
import ru.vit.sandbox.common.services.IXmlService;
import ru.vit.sandbox.utils.XmlValidator;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by v.gryazev on 03.04.2017.
 */
@Path("/")
public class XmlService implements IXmlService {
    final static Logger logger = Logger.getLogger(XmlService.class);

    @Override
    public Response test() {
        return Response.status(200).entity("service OK").build();
    }

    @Override
    public Response validate(XmlInfo xmlInfo) {
        XmlResult result = new XmlResult(xmlInfo);
        XmlValidator validator = new XmlValidator();
        result.setValidateResult(validator.validate(xmlInfo.getXml(), xmlInfo.getXsd()));
        result.setTransformResult(validator.transform(xmlInfo.getXml(),xmlInfo.getXsl()));
        return Response.ok(result, MediaType.APPLICATION_JSON).encoding("UTF-8").build();
    }

    @Override
    public Response transform(XmlInfo xmlInfo) {
        XmlResult result = new XmlResult(xmlInfo);
        result.setTransformResult(new XmlValidator().transform(xmlInfo.getXml(),xmlInfo.getXsl()));
        return Response.ok(result, MediaType.APPLICATION_JSON).encoding("UTF-8").build();
    }
}