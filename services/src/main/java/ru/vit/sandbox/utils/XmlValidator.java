package ru.vit.sandbox.utils;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import ru.vit.sandbox.common.entities.ResultOfSmth;

import javax.xml.XMLConstants;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by v.gryazev on 03.04.2017.
 */
public class XmlValidator {
    final static Logger logger = Logger.getLogger(XmlValidator.class);

    public ResultOfSmth validate (String xml, String xsd) {
        ResultOfSmth result = new ResultOfSmth();
        StreamSource xmlFile = new StreamSource(new StringReader(xml));
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema;
        try {
            schema = schemaFactory.newSchema(new StreamSource(new StringReader(xsd)));
        } catch (SAXException e) {
            result.setOk(false);
            result.setResult("XSD не валидна по причине: " + e);
            return result;
        }
        if (schema==null) {
            result.setOk(false);
            result.setResult("Возникла ошибка при чтении XSD.");
            return result;
        }
        Validator validator = schema.newValidator();
        try {
            validator.validate(xmlFile);
            result.setOk(true);
            result.setResult("Валидация успешно пройдена.");
            return result;
        } catch (Exception e){
            result.setOk(false);
            result.setResult("XML не валидна по причине: " + e);
            return result;
        }
    }

    public ResultOfSmth transform(String xml, String xslt) {
        ResultOfSmth result = new ResultOfSmth();
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = factory.newTransformer(new StreamSource(new StringReader(xslt)));
        } catch (TransformerConfigurationException e) {
            result.setOk(false);
            result.setResult("Ошибка при преобразовании: " + e);
            return result;
        }
        try {
            StringWriter outWriter = new StringWriter();
            Result streamResult = new StreamResult(outWriter);
            StreamSource xmlFile = new StreamSource(new StringReader(xml));
            transformer.transform(xmlFile, streamResult);
            StringBuffer sb = outWriter.getBuffer();
            result.setOk(true);
            result.setResult(sb.toString());
            return result;
        } catch (TransformerException e) {
            result.setOk(false);
            result.setResult("Ошибка при преобразовании: " + e);
            return result;
        }
    }
}
